<!DOCTYPE html>
<html lang="en">
<?php
include "../vendor/autoload.php";
use DebugBar\StandardDebugBar;

$debugbar = new StandardDebugBar();
$debugbarRenderer = $debugbar->getJavascriptRenderer();

$debugbar["messages"]->addMessage("hello world!");
?>
<head>
 <?php echo $debugbarRenderer->renderHead() ?>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<title>Teknisi On - Jalur Terdekat</title>
<link rel="icon" href="../img/logo.png">
<style>
html, body, #map-canvas {
	width: 100%;
	height: 80%;
	//margin: 0px;
	//padding: 0px
}
a{
	cursor: pointer;
	text-decoration: underline;
}
</style>
<!-- CSS LIBRARY -->
    <link rel="stylesheet"  href="../css2/lib/font-awesome.min.css">
  
    <link rel="stylesheet"  href="../css2/lib/fotorama.css">

    <!-- MAIN STYLE -->
    <link rel="stylesheet" href="../css2/style.css">

  <!-- Stylesheets -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="../css/font-awesome.min.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="../css/jquery-ui.css">
	 <!-- Main stylesheet -->
  <link href="../css/style.css" rel="stylesheet">
  <!---Other StyleSheet-->
  <link href="../css/custom.css" rel="stylesheet">
  
  <link href="../css/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="../css/widgets.css" rel="stylesheet"> 
  <script src="../js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvuc7P8ScVcj7N2W9zM63IGZO2pJE3_us&language=id" type="text/javascript"></script>
<script>
// map
var poly = '';
var map;
var markeruser = '';
var markerdestination = '';

// boolean
var __global_user		 = false;
var __global_destination = false;
var update_timeout;

// tempat menaruh data sementara jalur teknisi
var temp_list_angkot = [];

/**
* INITIALIZE GOOGLE MAP
*/
function initialize() {	
	/* setup map */
	var mapOptions = {
		zoom: 13,
		center: new google.maps.LatLng(-8.5384745, 115.084527)
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
	/* create marker and line by click */
	google.maps.event.addListener(map, 'click', function(event) 
	{	
		icons = '../img/map-dijkstra/user_min.png';
		var location = event.latLng;	

		update_timeout = setTimeout(function()
		{
			if(__global_user == false){
				markeruser = new google.maps.Marker({
					position: location,
					map: map,
					icon: icons,
					draggable: true,
					title: 'Lokasi Anda',
				});
				
				// update 
				__global_user = true;
			}else{
				markeruser.setPosition(location);
			}

		}, 200); 

	});	

	// handle click and dblclick same time
	google.maps.event.addListener(map, 'dblclick', function(event) {       
		clearTimeout(update_timeout);
	});	
}

/** 
* proses memilih teknisi
*/
function choose_destination(value){
	// teks option
	var teks = $("#select_tujuan option:selected").text();
	
	// -- PILIH -- dipilih
	if(value == 'pilih') return false;
		
	// reset polyline
	if(poly != '') poly.setMap(null);
	
	
	// RESET ANGKOT SEBELUMNYA
	$(temp_list_angkot).each(function(w, x){
		// x = marker0, marker1 dst
		window[x].setMap(null);
	});			
	
	var location = JSON.parse(value);
	//if(location=='')alert("Teknisi Belog");
	icons = '../img/map-dijkstra/teknisi.png';
	
	if(__global_destination == false){
		markerdestination = new google.maps.Marker({
			position: location,
			map: map,
			icon: icons,
			draggable: false,
			title: 'TUJUAN : ' + teks,
		});
		
		__global_destination = true;
	}else{
		markerdestination.setPosition(location);
		markerdestination.setTitle('TUJUAN : ' + teks);
	}
}

/**
* GET JSON DIJSKTRA VIA AJAX
*/
function send_dijkstra(){
	
	if(markeruser == ''){
		alert('Lokasi Anda Belum Ditemukan ');
		return false;
	}
	if(markerdestination == ''){
		alert('Silakan Tentukan Teknisi Yang Anda Inginkan');
		return false;
	}
	//console.log(markeruser.position.lat());
	//console.log(markeruser.position.lng());
	now_koord_user 			= '{"lat": ' + markeruser.position.lat() + ', "lng": ' + markeruser.position.lng() + '}';
	now_koord_destination 	= '{"lat": ' + markerdestination.position.lat() + ', "lng": ' + markerdestination.position.lng() + '}';

	// loading
	$('#run_dijkstra').hide();
	$('#penggunaan').hide();
	$('#loading').show();
	
	$.ajax({
		method:"POST",
		url : "Main.php",
		data: {koord_user: now_koord_user, koord_destination: now_koord_destination},
		success:function(response){
			
			// remove loading
			$('#run_dijkstra').show();
			$('#penggunaan').show();
			$('#loading').hide();
						
			var json = JSON.parse(response);
			console.log(response);
			
			// RESET POLYLINE
			if(poly != '') poly.setMap(null);
			
			// menghapus data teknisi/ penghitung data teknisi sebelumnya
			$(temp_list_angkot).each(function(w, x){
				// x = marker0, marker1 dst
				window[x].setMap(null);
			});

			// ERROR ALGORITMA DIJKSTRA
			if(json.hasOwnProperty("error")) alert(json['error']['teks']);
			
			// proses menggambar jalur dijkstra
			/* setup polyline */
			var polyOptions = {				
				
				path: json['jalur_shortest_path'],
				geodesic: true,
				strokeColor: 'rgb(20, 120, 218)',
				strokeOpacity: 1.0,
				strokeWeight: 2,
			};			
			poly = new google.maps.Polyline(polyOptions);
			poly.setMap(map);
			
			
			$(json['angkot']).each(function(i, v)
			{
				
				no_angkot = JSON.stringify(v['no_angkot']);
				window['infowindow'+i] = new google.maps.InfoWindow({
					content: '<div>'+ no_angkot +'</div>'
				});
				
				// gambar koordinat jalur
				koordinat_angkot = v['koordinat_angkot'];
				window['marker'+i] = new google.maps.Marker({
					position: koordinat_angkot,
					map: map,
					title: 'title',
					icon: '../img/map-dijkstra/car.png'
				});
				
				// popup
				window['marker'+i].addListener('click', function() {
					window['infowindow'+i].open(map, window['marker'+i]);
				});
				
				// temporary list jalur
				temp_list_angkot[i] = 'marker'+i;
			});
		},
		error:function(er){
			alert('error: '+er);
			
			// remove loading
			$('#run_dijkstra').show();
			$('#loading').hide();
		}
	});	
}

/* load google maps v3 */
google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>

<body>
<?php echo $debugbarRenderer->render() ?>
<div class="navbar navbar-fixed-top bs-docs-nav" role="banner">
	<div class="conjtainer">
      <!-- Menu button for smallar screens -->
      <div class="navbar-header">
		  <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
			
		  </button>
		  <!-- Site name for smallar screens -->
		  
		</div>
		<!-- Navigation starts -->
      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">  
	  <ul class="nav navbar-nav">  
          <!-- Upload to server link. Class "dropdown-big" creates big dropdown 
      

          <!-- Upload to server link. Class "dropdown-big" creates big dropdown -->
          <li class="dropdown dropdown-big">
            <a href="facebook.com"><span class = "label label-info"><i class="fa fa-facebook"></i></span> Like us on Facebook</a>            
          </li>
          <!-- Sync to server link -->
          <li class="dropdown dropdown-big">
            <a href="twitter.com" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-primary"><i class="fa fa-twitter"></i></span> Follow is on Twitter</a>
            <!-- Dropdown -->

          </li>

        </ul>

        <!-- Search form -->
       
        <!-- Links -->
       <!---<ul class="nav navbar-nav pull-right">
         <!--- <li class="dropdown pull-right">            
           <a href="#login" data-toggle="modal">
              <i class="fa fa-user"></i> Log in 
            </a>		        
            <!-- Dropdown menu -->
           <!--- <ul class="dropdown-menu">
              <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="#"><i class="fa fa-cogs"></i> Settings</a></li>
              <li><a href="login.html"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </li>
          
        </ul>--->
      </nav> 

    </div>
	</div>
	<br />
<?php
include "Main.php";
$id_kerusakan=$_GET['id'];
//echo $id_kerusakan;
// koneksi
$m = new Main();
$koneksi = $m->koneksi;

// query
$sql 	= "SELECT t_teknisi.`id_teknisi`,t_teknisi.`foto`,t_keahlian_teknisi.`id_kerusakan`,t_teknisi.`alamat_perusahaan`,t_teknisi.`email`,t_teknisi.`nama_teknisi`,
t_teknisi.`no_hp`,t_teknisi.`status`, t_perangkat.`jenis_perangkat`,t_perangkat.`merk`,t_kerusakan.`detail_kerusakan`,t_teknisi.`latitude`
,t_teknisi.`longtitude`
FROM t_keahlian_teknisi JOIN t_teknisi ON t_keahlian_teknisi.`id_teknisi`=
t_teknisi.`id_teknisi` AND t_keahlian_teknisi.`id_kerusakan`='$id_kerusakan' AND t_teknisi.`status`!='Inactive' 
AND t_teknisi.`longtitude`!='' AND 
t_teknisi.`latitude`!='' JOIN t_kerusakan ON t_keahlian_teknisi.`id_kerusakan`=
t_kerusakan.`id_kerusakan` JOIN t_perangkat ON t_kerusakan.`id_perangkat`=t_perangkat.`id_perangkat`";
$query 	= mysqli_query($koneksi, $sql);

// select option
echo '<div class="col-lg-7">
		<p class = "body center">Tentukan Teknisi : </p><select class="form-control" id="select_tujuan" onchange="choose_destination(this.value)">';
echo '<option value="pilih">-- PILIH --</option>';
	while($fetch = mysqli_fetch_array($query, MYSQLI_ASSOC))
	{
		//$koordinat 		= $fetch['koordinat'];
		//$exp_koordinat 	= explode(',', $koordinat);
		$longtitude = $fetch['longtitude'];
		$latitude = $fetch['latitude'];
		$json_koordinat	= '{"lat": '.$latitude.', "lng": '.$longtitude.'}';
		
		echo "<option value='$json_koordinat'>$fetch[nama_teknisi]</option>";
	}
echo '</select></div>';
?>
					
					<br /><button onclick="send_dijkstra()" id='run_dijkstra' class="btn btn-success btn-md">CARI</button>
					<a href="#info-dijkstra" data-target="#info-dijkstra" data-toggle="modal"
					 class="btn btn-info btn-md" id='penggunaan'><i class="fa fa-check-circle-o">Penggunaan</i>
					</a>
					<img src="../img/ajax-loader.gif" width="10px" height="10px" id="loading" style="display:none">
						
					
					<hr />
					<div id="map-canvas">
					</div>
					
		<div class = "content">
			<div class = "col-lg-12 col-md-12 col-xs-12 col-sm-12">
			
			<div class = "class = "col-lg-12">
				<h1 class = "page-title center">This site is accessible to</h1>
			</div>
			<div class = "row">
				<div class = "col-lg-12 center">
					<p class = "center pag-title">&nbsp;</p>
				</div>
			</div>
				<div class = "col-lg-4 col-md-4 center">
					<a href = ""><i class = "fa fa-windows fa-5x use"></i>
					<p class = "body center">Microsoft Windows</p>
					</a>
				</div>	
				<div class = "col-lg-4 col-md-4 center">
					<a href = ""><i class = "fa fa-android fa-5x use android"></i>
					<p class = "body center">Android</p>
					</a>
				</div>	
				<div class = "col-lg-4 col-md-4 center">
					<a href = ""><i class = "fa fa-apple fa-5x use apple"></i>
					<p class = "body center">Mac Os</p>
					</a>
				</div>	
			</div>
		</div>	
<div id="info-dijkstra" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title center"><i class = "fa fa-check-circle-o"></i>Bagaimana Cara Menggunakan Penentuan Jarak Terdekat ?</h4>
        </div>
         <div class="modal-body">
                  <div class="padd">
                  <p>
                  1. Tentukan salah satu teknisi yang akan Anda tuju, dengan cara pilih pada list yang sudah disediakan.<br />
				  2. Jika pada list teknisi pada halaman sebelumnya data teknisi ditemukan namun pada halaman ini tidak ditemukan, kemungkinan teknisi terebut belum melengkapi data lokasinya. <br />
				  3. Tentukan dimana lokasi anda berada saat ini, dengan cara klik satu kali pada layar google maps yang sudah disediakan. <br />
				  4. Tekan tombol "CARI" untuk melakukan proses penentuan jarak terdekat yang akan dihitung oleh sistem kami.<br />
				  5. Berikut adalah rute pencarian terdekat yang kami sediakan di Kabupaten Tabanan untuk dihitung oleh sistem kami, diluar jalur tersebut akan mengalami kendala dalam hasil penentuan jalur terdekat.<br />
				  <img src="../img/rute-dijkstra.png" class="img-thumbnail"/>
				  
				  Sekian dan Terimakasi. :)
				  </p>
				</div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
       	</div>
    </div>
</div>
</div>
<?php include '../footer.php';?> 	
<script src="../js/jquery.js"></script> <!-- jQuery -->
<script src="../js/bootstrap.min.js"></script> <!-- Bootstrap -->
<script src="../js/jquery-ui.min.js"></script> <!-- jQuery UI -->

</body>
</html>