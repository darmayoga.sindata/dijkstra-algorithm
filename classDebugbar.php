<?php
include "../vendor/autoload.php";
namespace DebugBar\Bridge;
use DebugBar\DataCollector\AssetProvider;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;
use DebugBar\DebugBarException;

class mysqlCollector extends DataCollector implements Renderable, AssetProvider
{
protected $mysql_queries;

public function __construct($mysql_queries)
{
    $this->mysql_queries = $mysql_queries; 
}

public function collect()
{  
    $queries = array();
    $totalExecTime = 0;
    foreach ($this->mysql_queries as $q) {  
        
        $query = explode('----',$q); 
        $queries[] = array(
            'sql' => $query[0],
            'duration' => $query[1],
            'duration_str' => $this->formatDuration($query[1])
        );
        $totalExecTime += $query[1];
    } 
    return array(
        'nb_statements' => count($queries),
        'accumulated_duration' => $totalExecTime,
        'accumulated_duration_str' => $this->formatDuration($totalExecTime),
        'statements' => $queries
    );
}

public function getName()
{
    return 'mysql_queries';
}

public function getWidgets()
{
    return array(
        "database" => array(
            "icon" => "arrow-right",
            "widget" => "PhpDebugBar.Widgets.SQLQueriesWidget",
            "map" => "mysql_queries",
            "default" => "[]"
        ),
        "database:badge" => array(
            "map" => "mysql_queries.nb_statements",
            "default" => 0
        )
    );
}

public function getAssets()
{
    return array(
        'css' => 'widgets/sqlqueries/widget.css',
        'js' => 'widgets/sqlqueries/widget.js'
    );
}
public function query($statement)
{
    return $this->profileCall('query', $statement, func_get_args());
}
protected function profileCall($method, $sql, array $args)
{

        $result = call_user_func_array(array($this->mysql_queries, $method), $args);print_r($result);
   
    return $result;
}
  public function addExecutedStatement( $stmt)
{
    $this->mysql_queries = $stmt; 
}
}
?>